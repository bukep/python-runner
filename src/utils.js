const fs = require("fs/promises");

const removePath = async (path) => {
  try {
    const stat = await fs.stat(path);

    if (stat.isFile()) {
      return await fs.rm(path);
    }

    if (stat.isDirectory()) {
      return await fs.rmdir(path);
    }

    throw new Error("Unhandled exception");
  } catch (e) {
    if (e.code !== "ENOENT") {
      // No such file or directory. File really does not exist
      console.log("Exception fs.statSync (" + path + "): " + e);
      throw e; // Something else went wrong, we don't have rights, ...
    }
  }
};

const removeNewLineSymbol = (text) => {
  if (text[text.length - 1] !== "\n") return text;
  return text.slice(0, -1);
};

const handleError = (errorLines) => {
  const errorMessageLines = errorLines.split("\n").map(removeNewLineSymbol);

  const line = Number(errorMessageLines[0].match(/line ([\d]+)/)[1]);
  const position = errorMessageLines[1].length;
  const message = errorMessageLines[3];

  return {
    line,
    position,
    message,
  };
};

const waitStreamMessage = (stream) =>
  new Promise((resolve, reject) => {
    const chunks = [];
    stream.on("data", (chunk) => chunks.push(chunk));
    stream.on("error", (err) => reject(err));
    stream.on("end", () =>
      resolve(Buffer.concat(chunks).toString("utf8") || undefined)
    );
  });

const waitProcess = async (process) => {
  const [successMessage, errorMessage] = await Promise.all([
    waitStreamMessage(process.stdout),
    waitStreamMessage(process.stderr),
  ]);

  return { successMessage, errorMessage };
};

module.exports = {
  removePath,
  handleError,
  waitProcess,
};
