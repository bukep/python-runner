const express = require("express");
const fs = require("fs/promises");
const { spawn } = require("child_process");
const { v4: uuid } = require("uuid");

const { removePath, handleError, waitProcess } = require("./utils");

const scriptsFolder = "./.runner";

(async () => {
  // Remove scripts folder if exist
  await removePath(scriptsFolder);

  // Create scripts folder
  await fs.mkdir(scriptsFolder);

  express()
    .use(express.json())
    .post("/python", async (req, res) => {
      try {
        if (typeof req.body?.code !== "string") {
          return res.status(400).json({
            error: {
              message: "Request must have 'code' field of type string",
            },
          });
        }

        const filePath = `${scriptsFolder}/${uuid()}.py`;

        // Write code to temporary file
        await fs.writeFile(filePath, req.body.code, "utf8");

        // Run python
        const process = spawn("python", [filePath]);

        const processMessage = await Promise.race([
          waitProcess(process),
          new Promise((resolve) => setTimeout(() => resolve("timeout"), 1_000)),
        ]);

        // Exit python process
        process.kill();

        // Remove temporary file
        await fs.rm(filePath);

        if (processMessage === "timeout") {
          return res.status(408).json({
            error: { message: "Timeout exceeded 1 second limit" },
          });
        }

        const { successMessage, errorMessage } = processMessage;

        if (errorMessage) {
          return res.status(400).json({ error: handleError(errorMessage) });
        }

        return res.json({
          data: { stdout: successMessage?.split("\n")?.slice(0, -1) || [] },
        });
      } catch (error) {
        res.status(500).json({
          error: {
            message: error.toString(),
          },
        });
        console.error(error); // TODO: logger
      }
    })
    .listen(process.env.PORT || 3000, () =>
      console.log(`Application listening on port ${process.env.PORT || 3000}!`)
    );
})();
