FROM node:16.13.0

WORKDIR /opt/app

COPY . .

RUN yarn install

RUN apt-get update || : && apt-get install python -y

EXPOSE 3000

CMD ["yarn", "start"]